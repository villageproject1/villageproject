/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.villageproject;

/**
 *
 * @author BankMMT
 */
public class Villager {
    protected int str;
    protected int vit;
    protected int agi;
    protected int dex;
    protected int intel;
    protected int luk;
    protected String name;
    protected char gender;

    public Villager (String name,char gender, int str, int vit, int agi, int dex, int intel, int luk){
        this.name = name;
        this.gender = gender;
        this.str = str;
        this.vit = vit;
        this.agi = agi;
        this.dex = dex;
        this.intel = intel;
        this.luk = luk;
        System.out.println("Create Villager "+this.name+" success");
    }
    public String getName(){
        return name;
    }
    public char getGender(){
        return gender;
    }
    public int getStr(){
        return str;
    }
    public int getVit(){
        return vit;
    }
    public int getDex(){
        return dex;
    }
    public int getAgi(){
        return agi;
    }
    public int getIntel(){
        return intel;
    }
    public int getLuk(){
        return luk;
    }
    public void showStatus() {
        System.out.println("Name   : "+name);
        if (gender == 'M' || gender == 'm'){
            System.out.println("Gender : Male");
        }else if (gender == 'F' || gender == 'f'){
            System.out.println("Gender : Female");
        }else {
            System.out.println("Unknow");
        }
        System.out.println("Str : "+str+"     (Strength)");
        System.out.println("Vit : "+vit+"     (Vitality)");
        System.out.println("Agi : "+agi+"     (Agility)");
        System.out.println("Dex : "+dex+"     (Dexterity)");
        System.out.println("Int : "+intel+"     (Intelligent)");
        System.out.println("Luc : "+luk+"     (Luck)");
        System.out.println("-----------------------------------");
    }
    public void learnWeapon() {
        str++;
        System.out.println("leanWeapon success");
    }
    public void tryToCutLog() {
        vit++;
        System.out.println("tryToCutLog success");
    }
    public void meditate() {
        dex++;
        System.out.println("meditate success");
    }
    public void jogging() {
        agi++;
        System.out.println("jogging success");
    }
    public void readBook() {
        intel++;
        System.out.println("readBook success");
    }

}
